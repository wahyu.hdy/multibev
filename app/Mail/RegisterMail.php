<?php

namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    public $verification_code;
    public $type;

    public function __construct($verification_code, $type)
    {
        $this->verification_code = $verification_code;
        $this->type = $type;
    }

    public function build()
    {
        $data['title'] = 'Multibev';
        $data['type']  = $type;
        $data['verification_code']  = $this->verification_code;

        return $this->subject('Multibev: Verification Account')->view('email.auth.email-verification', $data);
    }
}