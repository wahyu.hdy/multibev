<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Cart;
use App\Mail\RegisterMail;
use DB;
use Auth;

class TransactionController extends Controller
{
    public function addToCart($product_id)
    {
        DB::beginTransaction();
        try {

            $product = Product::find($product_id);

            $cart = Cart::create([
                'type'         => 'cart',
                'user_id'      => Auth::user()->id,
                'total_weight' => 0,
                'total_price'  => 0,
            ]);

            $cart_detail = CartDetail::create([
                'cart_id' => $cart->id,
                'product_id' => $product_id,
                'product_name' => $product->name,
                'product_price' => $product->price,
                'product_weight' => $product->product_weight,
                'qty' => 1,
                'created_at' => now(),
            ]);
            DB::commit();

            return redirect('/shopping-cart')->with('success', 'success add product to cart');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect('/shopping-cart')->with('failed', $th->getMessage());
        }
    }
}
