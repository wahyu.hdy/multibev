<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Mail\RegisterMail;
use DB;
use Auth;

class CustomerController extends Controller
{
    public function login()
    {
        return view('pages.Customer.login');
    }

    public function loginAction(Request $request)
    {
        $this->validate(request(), [
            'email'    => 'required|email',
            'password' => 'required|min:6'
        ]);

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return redirect('/login')->with('error', 'The email doesn\'t match our record');
        }

        if($user->verification_status != 1) {
            return redirect('/login')->with('error', 'The email account is not active');
        }

        $authenticated = Auth::attempt($request->only('email', 'password'));

        if ($authenticated) {
            
            $user = Auth::user();
            
            $user->timestamps = false;
            $user->remember_token = str_random(100);
            $user->save();

            return redirect('/');

        } else {
            return redirect('/login')->with('error', 'The password doesn\'t match our record');
        }

    }

    public function register()
    {
        return view('pages.Customer.register');
    }

    public function registerAction(Request $request)
    {
        $this->validate($request,[
            'firstname'             => 'required',
            'lastname'              => 'required',
            'email'                 => 'required|email|unique:users,email',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);

        DB::BeginTransaction();
        try {

            $name = $request->firstname . ' ' . $request->lastname;
            $verification_code = str_random(32);
            $save = User::create([
                'name'                => $name,
                'username'            => $this->randomUsername($name),
                'email'               => $request->email,
                'password'            => bcrypt($request->password),
                'verification_code'   => $verification_code,
                'verification_status' => 0,
                'created_at'          => now()
            ]);
    
            Mail::to($request->email)->send(new RegisterMail($verification_code, 'verify'));

            DB::commit();

            return redirect('/login')->with('success', 'Register success, please check your email!');

        } catch (\Throwable $th) {
            DB::rollback(); 

            return redirect('/register')->with('error', $th->getMessage());
        }
    }

    public function verification($hash)
    {
        $code = decrypt($hash);
        $user = User::where('verification_code', $code)->firstOrFail();
        if ($user) {
            $user->verification_status = 1;
            $user->save();
            Auth::login($user);

            return redirect('/login')->with('success', 'Thank you, your account has been verified.');
        } else {
            return redirect('/login')->with('error', 'Failed to verify please retry');
        }
    }

    private function randomUsername($string) {
        $pattern    = " ";
        $firstPart  = strstr(strtolower($string), $pattern, true);
        $secondPart = substr(strstr(strtolower($string), $pattern, false), 0,3);
        $nrRand     = rand(0, 100);
        
        $username = trim($firstPart).trim($secondPart).trim($nrRand);

        return $username;
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
