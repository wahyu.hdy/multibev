<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Post;
use App\Cart;
use App\CartDetail;
use App\User;
use App\Mail\RegisterMail;
use DB;
use Auth;

class ShoppingCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['carts'] = Cart::with(['details', 'details.product.images'])
            ->where('user_id', Auth::user()->id)->first();

        return view('pages.ShoppingCart.index', $data);
    }

    public function show($id)
    {
        $data['popular_posts'] = Post::with(['images'])->where('type', 'news')->orderBy('id', 'asc')->limit(4)->get();
        $data['receipe'] = Post::with(['images', 'category'])->where('type', 'news')->findOrFail($id);

        return view('pages.Recipes.detail', $data);
    }

    public function addToCart($product_id)
    {
        DB::beginTransaction();
        try {
            $cart_detail = CartDetail::with(['cart', 'product'])
                ->where('product_id', $product_id)
                ->whereHas('cart', function($q){
                    $q->where('user_id', Auth::user()->id);
                })->first();

            if ($cart_detail) {
                $cart_detail->update([
                    'qty'            => $cart_detail->qty + 1,
                    'updated_at'     => now(),
                ]);
            } else {

                $cart = Cart::where('user_id', Auth::user()->id)->first();
                $product = Product::find($product_id);

                if (!$cart) {
                    $cart = Cart::create([
                        'type'         => 'cart',
                        'user_id'      => Auth::user()->id,
                        'total_weight' => 0,
                        'total_price'  => 0,
                    ]);
                }
    
                $cart_detail = CartDetail::create([
                    'cart_id'        => $cart->id,
                    'product_id'     => $product_id,
                    'product_name'   => $product->name,
                    'product_price'  => $product->price,
                    'product_weight' => $product->product_weight,
                    'qty'            => request()->has('qty') ? request()->input('qty') : 1,
                    'created_at'     => now(),
                ]);
            }

            DB::commit();

            return redirect('/shopping-cart')->with('success', 'success add product to cart');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect('/shopping-cart')->with('failed', $th->getMessage());
        }
    }

    public function countCart()
    {
        $cart = Cart::select('id')->where('user_id', Auth::user()->id)->first();

        $cart_detail = CartDetail::select(DB::raw('count(id) as cart_total'))->where('cart_id', $cart->id)->first();
        
        return $cart_detail;
    }

    public function updateQtyCart(Request $request) 
    {
        $this->validate(request(), [
            'cart_id'    => 'required|exists:carts,id',
            'product_id' => 'required|exists:products,id',
            'qty'        => 'required|numeric'
        ]);

        $cart_detail = CartDetail::where('cart_id', $request->cart_id)
            ->where('product_id', $request->product_id)
            ->first();

        $product = Product::find($request->product_id);

        $response = [
            'status' => false,
            'product_price' => null
        ];
        if ($cart_detail) {
            $update = $cart_detail->update([
                'qty' => $request->qty,
                'product_price' => $product->price * $request->qty,
                'product_weight' => $product->product_weight * $request->qty,
            ]);

            $response = [
                'status' => true,
                'product_price' => number_format($product->price * $request->qty)
            ];
        }

        return $response;
    }
    
    public function deleteProductCart($cart_id, $product_id) 
    {
        $cart_detail = CartDetail::where('cart_id', $cart_id)
            ->where('product_id', $product_id)
            ->first();

        if ($cart_detail) {
            $cart_detail->delete();
        }

        return redirect('/shopping-cart');
    }

    public function deleteCart($cart_id) 
    {
        $cart= Cart::find($cart_id);

        if ($cart) {
            $cart->delete();
        }

        return redirect('/shopping-cart');
    }
}
