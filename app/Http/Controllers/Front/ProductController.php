<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Image;
use DB;

class ProductController extends Controller
{
    public function index($id)
    {
        $product = (new Product)->newQuery();

        $q          = '';
        $page       = request()->except('page');
        $per_page   = request()->has('per_page') ? request()->per_page : 6;
        $sort_field = request()->has('sort_field') ? strtolower(request()->sort_field) : 'name';
        $sort_order = request()->has('sort_order') ? strtolower(request()->sort_order) : 'asc ';

        if (request()->has('search')) {
            $q = strtolower(request()->input('search'));
            $product->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $product->orderBy($sort_field, $sort_order);
        } else {
            $product->orderBy('id', 'desc');
        }

        $product = $product->paginate($per_page)
            ->appends($page);

        $data['sort_field']     = $sort_field;
        $data['sort_order']     = $sort_order;
        $data['per_page']       = $per_page;
        $data['categories']     = Category::with(['childs'])->where('parent_id', 0)->get();
        $data['products']       = $product;
        $data['product_slides'] = Image::where('type', 'product_slide')->get();

        return view('pages.Product.index', $data);
    }

    public function list() 
    {
        $product = (new Product)->newQuery();

        $q          = '';
        $page       = request()->except('page');
        $per_page   = request()->has('per_page') ? request()->per_page : 6;
        $sort_field = request()->has('sort_field') ? strtolower(request()->sort_field) : 'name';
        $sort_order = request()->has('sort_order') ? strtolower(request()->sort_order) : 'asc ';

        if (request()->has('search')) {
            $q = strtolower(request()->input('search'));
            $product->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $product->orderBy($sort_field, $sort_order);
        } else {
            $product->orderBy('id', 'desc');
        }

        $product = $product->paginate($per_page)
            ->appends($page);

        $data['sort_field']     = $sort_field;
        $data['sort_order']     = $sort_order;
        $data['per_page']       = $per_page;
        $data['categories']     = Category::with(['childs'])->where('parent_id', 0)->get();
        $data['products']       = $product;
        $data['product_slides'] = Image::where('type', 'product_slide')->get();

        return view('pages.Product.index', $data);
    }

    public function show($id)
    {
        $data['product'] = Product::with('images')->findOrFail($id);
        $data['products'] = Product::with('images')->limit(4)->get();

        return view('pages.Product.detail', $data);
    }
}
