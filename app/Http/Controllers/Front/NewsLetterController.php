<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Newsletter;

class NewsLetterController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|unique:newsletters,email'
        ]);

        $save = Newsletter::create([
            'email' => $request->email,
            'created_at' => now()
        ]);

        return redirect()->back()->with('message', 'success subscribed');
    }
}
