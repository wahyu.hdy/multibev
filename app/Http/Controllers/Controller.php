<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Category;
use App\Menu;
use App\Config;
use App\Image;
use App\Cart;

Use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        // Load your objects
        $categories = Category::where('parent_id', 0)->where('type', 'product')->get();
        $menus = Menu::all();

        foreach($menus as $menu) {
            $menu_contents[$menu->type][] = $menu;
        }

        $configs = Config::whereIn('name', [
            'company_logo_header', 'company_phone', 'company_address', 'company_email', 
            'company_whatsapp', 'company_facebook', 'company_instagram', 'company_twitter'
        ])->pluck('value');

        $payment_methods = Image::where('type', 'footer_payment')->pluck('url');

        // Make it available to all views by sharing it
        view()->share('categories_menu', $categories);
        view()->share('menus', $menu_contents);
        view()->share('configs', $configs);
        view()->share('payment_methods', $payment_methods);
    }
}
