function alertHapus(judul,pesan,tujuan){
    $.confirm({
        title: judul,
        content: '<div style="text-align:left;">'+pesan+'</div>',
        type:'blue',
        buttons: {
            Ya: {
                text: '<i class="fa fa-check-circle"></i> Ya',
                btnClass: 'btn-blue',
                action: function () {
                    window.location = tujuan;
                }
            },
            Tidak: {
                text: '<i class="fa fa-times-circle"></i> Tidak',
                btnClass:'btn btn-danger btn-sm'
            }
        }
    });
}