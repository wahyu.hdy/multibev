<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Selamat datang di Multibev</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Default Description">
    <meta name="keywords" content="E-commerce">
    <meta name="robots" content="*">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="#" type="image/x-icon">
    <link rel="shortcut icon" href="#" type="image/x-icon">
    <link rel="icon" href="images/logo/logo.png">

    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/revslider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.bxslider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.mobile-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/product.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/flaticon.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style_extra.css') }}" media="all">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:700,600,800,400' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i,900" rel="stylesheet">
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('fonts/icomoon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('js/confirm/css/jquery-confirm.css') }}" type="text/css" />
</head>

<body>
    <!-- <div id="preloader"></div> -->

    <!-- STYLE SWITCHER  ============================================= -->
    <div id="stlChanger">
        <div class="blockChanger bgChanger">
            <a href="https://wa.me/{{$configs[0]}}" class="chBut icon-xs" target="_blank">
                <img src="{{ asset('images/whatsapp_PNG21.png') }}" alt="logo" style="width: 70px;padding-top: 3px;">
            </a>
        </div>
    </div> <!-- END SWITCHER -->
    <div id="page">

        @include("partials.navbar")

        @yield('content')

        @include("partials.footer")

    </div>

    @include("partials.mobilemenu")
    
    @yield('before_main_script')

    <!-- JavaScript -->
    <!-- Main -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/confirm/js/jquery-confirm.js') }}"></script>
    <script src="{{ asset('js/confirm/js/getConfirm.js') }}"></script>

    @yield('after_main_script')

    @if(isset(Auth::user()->id))
    <script>
        jQuery(document).ready(function() {
            jQuery.get("{{url('count-cart')}}", function(data, status){
                jQuery('#cartTotal').append(data.cart_total);
            });
        });
    </script>
    @endif

</body>

</html>
