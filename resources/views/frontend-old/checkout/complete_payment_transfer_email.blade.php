
	<div class="content-area mb-5 pt-4 order-summary">
			<div class="container container-product ">
				<div class="row">
					<div class="col-md-7 mx-auto">
						<h3 class="mb-4 bold-300">Finish Payment</h3>
						<p class="mb-3">
							Please finish your payment before there is no stock left.
							{{-- Segera selesaikan pembayaran Anda sebelum stok habis. --}}
						</p>
						
						<div class="row align-items-end mb-4">				
							<div class="col-12 float-left mt-3 cl-abu label-12">
								<p>
									* Make sure not to inform your payment evidence and data to any party except Talasi
									{{-- * Pastikan untuk tidak menginformasikan bukti dan data pembayaran kepada pihak manapun kecuali Talasi --}}
								</p>
							</div>
						</div>
						<div class="row align-items-end mb-4">				
							<div class="col-12 float-left mt-3 cl-abu label-12">
								<p>
									 <strong>ALAMAT PENGIRIMAN</strong><br />
                                       @foreach ($customer as $detail_cs)
											 {{ $detail_cs->postal_code }}<br />
									         {{ $detail_cs->phone_number }}<br />
											 {{ $detail_cs->address }}
									   @endforeach									
								</p>
							</div>
						</div>
						<div class="row align-items-end mb-4">				
							<div class="col-12 float-left mt-3 cl-abu label-12">
								<p>
									 <strong>Detail Order</strong>
									 <table cellspacing='1' style='border: 0.5px solid #FFE4E1; color:#000; text-align: left; width: 80%; font-size: 14px; font-family: Helvetica, Arial, sans-serif'; align='center'>
										<tr bgcolor='#ccc' align='left' style='border: 0.8px solid #FFE4E1; padding: 20px 30px 20px 30px; color: #444; font-size: 18px;'>
											<td>Product Name</td>
											<td>QTY</td>
											<td>Price</td>
										</tr>
										@foreach ($order_dtl as $detail_od)
										<tr style='border: 0.4px solid #FFE4E1; padding: 20px 30px 20px 30px; color: #444; font-size: 14px;'>
											<td>{{ $detail_od->name }}</td>
											<td>{{ $detail_od->quantity }}</td>
											<td> {{ currency_format($detail_od->price) }}</td>
										</tr>
										@endforeach	
										<tr>
										    <td>Kurir</td>
										    <td>&nbsp;</td>
										    <td>{{ currency_format($order->total_shipping_cost) }}</td>
										</tr>
										<tr>
										    <td>Total Bayar</td>
										    <td>&nbsp;</td>
										    <td>{{ currency_format($order->grand_total) }}</td>
										</tr>
									 </table>                                       						
								</p>
							</div>
						</div><hr />
						
						<div class="bank-account d-inline-flex">
							<img src="https://talasi.co.id/assets/img/bank/bca.png" width="120px" alt="">							
							<p class="mt-1" id="clipboard"><b>8691455998</b></p>
						</div>
						<p class="mt-2">
						    Transfer pembayaran ke nomor rekening BCA: <br />
							a/n TALASI BUMI TABANAN PT
						</p>
					</div>
				</div>
			</div>
		</div>
	

