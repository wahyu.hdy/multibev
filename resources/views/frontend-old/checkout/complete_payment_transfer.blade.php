@extends('frontend/layouts/main')

@section('custom_css')
<link href="{{ asset('assets/css/style-ricko.css') }}" rel="stylesheet" />
<style>
#clipboard {
	font-size: 18px;
}
</style>
@endsection

@section('content')
	<div class="content-area mb-5 pt-4 order-summary">
			<div class="container container-product ">
				<div class="row">
					<div class="col-md-7 mx-auto">
						<h3 class="mb-4 bold-300">Finish Payment</h3>
						<p class="mb-3">
							Please finish your payment before there is no stock left.
							{{-- Segera selesaikan pembayaran Anda sebelum stok habis. --}}
						</p>
						
						<div class="row align-items-end mb-4">				
							<div class="col-12 float-left mt-3 cl-abu label-12">
								<p>
									* Make sure not to inform your payment evidence and data to any party except Talasi
									{{-- * Pastikan untuk tidak menginformasikan bukti dan data pembayaran kepada pihak manapun kecuali Talasi --}}
								</p>
							</div>
						</div>
						<div class="row align-items-end mb-4">				
							<div class="col-12 float-left mt-3 cl-abu label-12">
								<p>
									 <strong>ALAMAT PENGIRIMAN</strong><br />
                                       @foreach ($customer as $detail_cs)
											 {{ $detail_cs->postal_code }}<br />
									         {{ $detail_cs->phone_number }}<br />
											 {{ $detail_cs->address }}
									   @endforeach									
								</p>
							</div>
						</div>
						<div class="row align-items-end mb-4">				
							<div class="col-12 float-left mt-3 cl-abu label-12">
								<p>
									 <strong>Detail Order</strong>
									 <table cellspacing='0' style='border: 0.5px solid #FFE4E1; color:#000; text-align: left; width: 80%; font-size: 14px; font-family: Helvetica, Arial, sans-serif'; align='center'>
										<tr bgcolor='#fafafa' align='left' style='padding: 20px 30px 20px 30px; color: #444; font-size: 14px;'>
											<td>Product Name</td>
											<td>QTY</td>
											<td>Price</td>
										</tr>
										@foreach ($order_dtl as $detail_od)
										<tr>
											<td>{{ $detail_od->name }}</td>
											<td>{{ $detail_od->quantity }}</td>
											<td> {{ currency_format($detail_od->price) }}</td>
										</tr>
										@endforeach	
										<tr>
										    <td>Kurir</td>
										    <td>&nbsp;</td>
										    <td>{{ currency_format($order->total_shipping_cost) }}</td>
										</tr>
									 </table>                                       						
								</p>
							</div>
						</div>
						<p class="bold-700">
							Transfer pembayaran ke nomor rekening BCA: 
						</p>
						<div class="bank-account d-inline-flex">
							<img src="{{ asset('assets/img/bank/bca.png') }}" alt="">							
							<p class="mt-1" id="clipboard">8691455998</p>
						</div>
						<p class="mt-2">
							a/n TALASI BUMI TABANAN PT
						</p>
						
						<a href="#" onclick="copyToClipboard('#clipboard')" class="text-small link-copy" title="">Salin No. Rek</a>

						<div class="mt-md-5 pt-md-5 mt-2 pt-2 border-top border-grey">
							<div class="row align-items-end">
								<div class="col-md-6">
									<p class="bold-700">
										Amount to be paid
									</p>

									<h5 class="total-color bold-700 mb-3">{{ currency_format($order->grand_total) }}</h5>
									<span id="nominal" class="d-none">413111</span>
								</div>
							</div>
							<div class="w-100 float-left mt-3 cl-abu label-12">
								{{-- <p>Silahkan melakukan konfirmasi apabila anda telah menyelesaikan pembayaran.</p> --}}
								<a href="{{ route('frontend.product.purchase') }}"><button type="button" class="btn btn-primary btn-send-about">Continue shopping</button></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	

@endsection

@section('footer')
@include('frontend/includes/footer')
@endsection

@section('custom_js')

<script>

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}



// ------------ batas


// Set the date we're counting down to
@php
if ($order->payment_method == 'gopay') {
	$date = \Carbon\Carbon::parse($order->created_at)->addMinutes(15);
} else {
	$date = \Carbon\Carbon::parse($order->created_at)->addHours(24);
	// if ($order->multi) {
	// 	$date = \Carbon\Carbon::parse($order->multiResponse->payValidDt . $order->multiResponse->payValidTm);
	// } else {
	// 	$date = \Carbon\Carbon::parse($order->response->payValidDt . $order->response->payValidTm);
	// }
}
@endphp
var countDownDate = new Date("{{ $date->format('m d, Y H:i:s') }}").getTime();
var dateNow = Date.now();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Output the result in an element with id="demo"
 if (dateNow > countDownDate){
	hours = '00';
	minutes = '00';
	seconds = '00';
 }
  document.getElementById("days").innerHTML = days;
  document.getElementById("hours").innerHTML = hours;
  document.getElementById("minutes").innerHTML = minutes;
  document.getElementById("seconds").innerHTML = seconds;

  // If the count down is over, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
</script>

@endsection
