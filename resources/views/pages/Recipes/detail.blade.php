@extends("Layouts.master")
@section('content')

    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title">
                        <h2>Product List</h2>
                    </div>
                </div>
                <!--col-xs-12-->
            </div>
            <!--row-->
        </div>
        <!--container-->
    </div>
    <!--breadcrumbs-->
    <!-- BEGIN Main Container -->
    <div class="main-container col2-left-layout">
        <div class="main container">
            <div class="row">
                <div class="col-left sidebar col-sm-3 blog-side">
                    <div id="secondary" class="widget_wrapper13" role="complementary">
                        <div id="recent-posts-4"
                            class="popular-posts widget widget__sidebar wow bounceInUp animated animated"
                            style="visibility: visible;">
                            <h2 class="widget-title">Most Popular Posts</h2>
                            <div class="widget-content">
                                <ul class="posts-list unstyled clearfix">
                                    
                                    @foreach($popular_posts as $popular_post)
                                    <li>
                                        <figure class="featured-thumb"> <a href="#"> <img
                                                    src="{{ url($popular_post->images[0]->url) }}" alt="blog image"> </a>
                                        </figure>
                                        <!--featured-thumb-->
                                        <div class="content-info">
                                            <h4><a href="{{ url('/recipes/'.$popular_post->id) }}"
                                                    title="Lorem ipsum dolor sit amet">
                                                {{$popular_post->title_en}}
                                                </a>
                                            </h4>
                                            <p class="post-meta">
                                                <time class="entry-date">{{$popular_post->created_at}}</time>
                                            </p>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!--widget-content-->
                        </div>
                    </div>
                </div>
                <div class="col-main col-sm-9 main-blog">
                    <!--<div class="page-title"><h2></h2></div>-->
                    <div id="main" class="blog-wrapper">

                        <div id="primary" class="site-content">
                            <div id="content" role="main">
                                <article id="post-29" class="blog_entry clearfix">

                                    <div>
                                        <div class="featured-thumb"><a href="#"><img src="{{ url($receipe->images[0]->url) }}"
                                                    alt="blog-img4"></a></div>
                                        <header class="blog_entry-header clearfix">
                                            <div class="blog_entry-header-inner">
                                                <h1 class="blog_entry-title">{{$receipe->title_en}}</h1>
                                            </div>
                                            <!--blog_entry-header-inner-->
                                        </header>
                                        <!--blog_entry-header clearfix-->

                                        <div class="entry-content">
                                            <ul class="post-meta">
                                                <!-- <li><i class="fa fa-user"></i>posted by <a href="#">admin</a> </li> -->
                                                <!-- <li><i class="fa fa-comments"></i><a href="#">8 comments</a> </li> -->
                                                <li><i class="fa fa-clock-o"></i>
                                                    <span class="month">{{$receipe->created_at}}</span></li>
                                            </ul>
                                            {!!$receipe->content_en!!}
                                        </div>
                                    </div>

                                </article>

                            </div>
                        </div>

                    </div>
                    <!--#main wrapper grid_8-->
                </div>
                <!--col-main col-sm-9-->
            </div>
        </div>
        <!--main-container-->
    </div>
    <!--col2-layout-->

@endsection

@section('after_main_script')
    
    <script src="{{ asset('js/parallax.js') }}"></script>
    <script src="{{ asset('js/revslider.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
    <script src="{{ asset('js/jquery.flexslider.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mobile-menu.min.js') }}"></script>

@endsection