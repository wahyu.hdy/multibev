@extends("Layouts.master")
@section('content')

    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title">
                        <h2>Login or Create an Account</h2>
                    </div>
                </div>
                <!--col-xs-12-->
            </div>
            <!--row-->
        </div>
        <!--container-->
    </div>


    <!-- BEGIN Main Container -->

    <div class="main-container col1-layout wow bounceInUp animated animated" style="visibility: visible;">

        <div class="main">
            <div class="account-login container">
                <!--page-title-->
                <fieldset class="col2-set">
                    <div class="col-1 new-users">
                        <strong>Registered Customers</strong>
                        <div class="content">

                            <p>If you have an account with us, please log in.</p>
                            <div class="buttons-set">
                                <button type="button" title="Create an Account" class="button login" onClick="">
                                    <a href="{{ url('/login') }}">
                                        <span><span>Login</span></span>
                                    </a>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-2 registered-users">
                        <strong>Registered Customers</strong>
                        <div class="content">

                            <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                            
                            @if(session()->has('success'))
                                <p style="color: blue">{{ session()->get('success') }}</p>
                            @endif

                            @if(session()->has('error'))
                                <p style="color: red">{{ session()->get('error') }}</p>
                            @endif

                            <form action="{{ url('/register-action') }}" method="POST">
                                {{ csrf_field() }}
                                <ul class="form-list">
                                    <li>
                                        <label for="email">First Name<em class="required">*</em></label>
                                        <div class="input-box">
                                            <input type="text" name="firstname" value="{{ old('firstname') }}" id="email"
                                                class="input-text required-entry validate-email" title="Email Address">
                                        </div>
                                        @if($errors->has('firstname'))
                                            <span class="required">{{ $errors->first('firstname') }}</span>
                                        @endif
                                    </li>
                                    <li>
                                        <label for="email">Last Name<em class="required">*</em></label>
                                        <div class="input-box">
                                            <input type="text" name="lastname" value="{{ old('lastname') }}" id="email"
                                                class="input-text required-entry validate-email" title="Email Address">
                                        </div>
                                        @if($errors->has('lastname'))
                                            <span class="required">{{ $errors->first('lastname') }}</span>
                                        @endif
                                    </li>
                                    <li>
                                        <label for="email">Email<em class="required">*</em></label>
                                        <div class="input-box">
                                            <input type="text" name="email" value="{{ old('email') }}" id="email"
                                                class="input-text required-entry validate-email" title="Email Address">
                                        </div>
                                        @if($errors->has('email'))
                                            <span class="required">{{ $errors->first('email') }}</span>
                                        @endif
                                    </li>
                                    <li>
                                        <label for="pass">Password<em class="required">*</em></label>
                                        <div class="input-box">
                                            <input type="password" name="password" class="input-text required-entry validate-password" 
                                                id="pass" title="Password">
                                        </div>
                                        @if($errors->has('password'))
                                            <span class="required">{{ $errors->first('password') }}</span>
                                        @endif
                                    </li>
                                    <li>
                                        <label for="pass">Re-type Password<em class="required">*</em></label>
                                        <div class="input-box">
                                            <input type="password" name="password_confirmation" class="input-text required-entry validate-password" 
                                                id="pass" title="Password">
                                        </div>
                                        @if($errors->has('password_confirmation'))
                                            <span class="required">{{ $errors->first('password_confirmation') }}</span>
                                        @endif
                                    </li>
                                </ul>

                                <div class="buttons-set">
                                    <button type="submit" class="button create-account" title="Login" name="send">
                                        <span>Create Account</span>
                                    </button>
                                </div>
                            </form>
                            <!--buttons-set-->
                        </div>
                        <!--content-->
                    </div>
                    <!--col-2 registered-users-->
                </fieldset>
                <!--col2-set-->

            </div>
            <!--account-login-->

        </div>
        <!--main-container-->

    </div>
    <!--col1-layout-->
    <div class="container">
        <div class="row our-features-box">
            <ul>
                <li>
                    <div class="feature-box">
                        <div class="icon-truck"></div>
                        <div class="content">FREE SHIPPING on order over $99</div>
                    </div>
                </li>
                <li>
                    <div class="feature-box">
                        <div class="icon-support"></div>
                        <div class="content">Have a question?<br>
                            +1 800 789 0000</div>
                    </div>
                </li>
                <li>
                    <div class="feature-box">
                        <div class="icon-money"></div>
                        <div class="content">100% Money Back Guarantee</div>
                    </div>
                </li>
                <li>
                    <div class="feature-box">
                        <div class="icon-return"></div>
                        <div class="content">30 days return Service</div>
                    </div>
                </li>
                <li class="last">
                    <div class="feature-box">
                        <div class="icon-leaf"></div>
                        <div class="content">Fresh & Healthy organic </div>
                    </div>
                </li>

            </ul>
        </div>
    </div>
    <!-- For version 1,2,3,4,6 -->

@endsection

@section('after_main_script')

    <script src="{{ asset('js/parallax.js') }}"></script>
    <script src="{{ asset('js/revslider.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
    <script src="{{ asset('js/jquery.flexslider.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mobile-menu.min.js') }}"></script>

@endsection
