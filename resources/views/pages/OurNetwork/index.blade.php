@extends("Layouts.master")
@section('content')

    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title">
                        <h2>Our Network</h2>
                    </div>
                </div>
                <!--col-xs-12-->
            </div>
            <!--row-->
        </div>
        <!--container-->
    </div>
    <!--breadcrumbs-->
    <!-- BEGIN Main Container -->
    <div class="main-container col2-left-layout">
        <div class="main container">
            <div class="row">

                <!-- Start about Area -->
                <section class="section-gap info-area" id="our_network">
                    <div class="container">
                        <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1cPYuEBnjwFk-ayMd3ftKLaVlRYqFv4Xe"
                            width="100%" height="800"></iframe>
                    </div>
                </section>
                <!-- End about Area -->
            </div>
        </div>
        <!--main-container-->

    </div>
    <!--col2-layout-->

@endsection

@section('after_main_script')
    
    <script src="{{ asset('js/parallax.js') }}"></script>
    <script src="{{ asset('js/revslider.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
    <script src="{{ asset('js/jquery.flexslider.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mobile-menu.min.js') }}"></script>

@endsection