@extends("Layouts.master")
@section('content')

<div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
       <div class="page-title"><h2>About</h2></div>
        </div>
        <!--col-xs-12-->
      </div>
      <!--row-->
    </div>
    <!--container-->
  </div>    	
<!--breadcrumbs--> 
<!-- BEGIN Main Container -->  
       <div class="main-container col2-left-layout">
	       <div class="main container"> 
	        <div class="row">                          
                         
              <!-- Start about Area -->
              <section class="section-gap info-area" id="about">
                <div class="container">					
                  <div class="single-info row mt-40">
                    <div class="col-lg-6 col-md-12 mt-120 text-center no-padding info-left">
                      <div class="info-thumb">
                        <img src="{{ url($about->url) }}" class="img-fluid" alt="" style="margin-top: 30px;">
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-12 no-padding info-rigth">
                      <div class="info-content">
                        <h2 class="pb-30">{{$about->title_en}}</h2>
                        <p>{!!$about->content_en!!}</p>  
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <!-- End about Area -->
            </div>    
	       </div><!--main-container-->

        </div> <!--col2-layout-->
     
        <section class="ftco-section ftco-degree-bg" style="text-align: center;">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 ftco-animate">
                <h2 class="mb-3">{{ $mission->title_en }}</h2>
                <p>{!! $mission->content_en !!}</p>
                </br>
                <p>
                  <img src="{{ isset($mission) ? url($mission->url) : '' }}" alt="" class="img-fluid">
                </p>
              </div>
            </div>
          </div>
        </section>

@endsection

@section('after_main_script')
    
  <script src="{{ asset('js/parallax.js') }}"></script>
  <script src="{{ asset('js/revslider.js') }}"></script>
  <script src="{{ asset('js/common.js') }}"></script>
  <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
  <script src="{{ asset('js/jquery.flexslider.js') }}"></script>
  <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('js/jquery.mobile-menu.min.js') }}"></script>

@endsection