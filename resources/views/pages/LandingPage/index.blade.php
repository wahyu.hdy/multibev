@extends("Layouts.master")
@section('content')

<div class="content">
  <div id="thmg-slider-slideshow" class="thmg-slider-slideshow">
      <div class="container">
          <div id='thm_slider_wrapper' class='thm_slider_wrapper fullwidthbanner-container' >
              <div id='thm-rev-slider' class='rev_slider fullwidthabanner'>
                  <ul>
                      
                      @foreach($slideshows as $slideshow)
                      <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='{{$slideshow->url}}'>
                            <img src='{{$slideshow->url}}'  data-bgposition='center top'  data-bgfit='cover' data-bgrepeat='no-repeat' 
                                alt="slider-image1" />
                          <div class="info">
                            <!-- <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-x='0'  data-y='180'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap;'><span>{{$slideshow->title_en}}</span></div> -->
                            <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-x='0'  data-y='260'  data-endspeed='500'  data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3; white-space:nowrap;'><span>{{$slideshow->title_en}}</span></div>
                            <!-- <div class='tp-caption sfb  tp-resizeme ' data-x='0'  data-y='480'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'><a href='#' class="buy-btn">Shop Now</a></div> -->
                            <div class='tp-caption Title sft  tp-resizeme ' data-x='0'  data-y='380'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'>{{$slideshow->content_en}}</div>
                          </div>
                      </li>
                      @endforeach
                  </ul>
              </div>
          </div>
      </div>
  </div>
</div>

<!--Category slider Start-->
<div class="top-cate">
  <div class="featured-pro container">
      <div class="row">
          <div class="slider-items-products">
              <div id="top-categories" class="product-flexslider hidden-buttons">
                  <div class="slider-items slider-width-col4 products-grid">
                      @foreach($categories as $category)
                      <div class="item"> 
                          <a href="{{ url('/product/'.$category->id) }}">
                              <div class="pro-img"><img src="{{ isset($category->cover_image) ? url($category->cover_image) : '' }}" alt="{{$category->title_en}}">
                                  <div class="pro-info"><h3>{{$category->title_en}}</h3></div>
                              </div>
                          </a> 
                      </div>
                      @endforeach
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<!--Category silder End-->

<div id="top">
  <div class="container">
      <div class="row">
          @foreach($home_products as $h_product)
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> 
              <a href="#" data-scroll-goto="1"> 
                  <img src="{{url($h_product->url)}}" alt="promotion-banner1"> 
              </a> 
          </div>
          @endforeach
      </div>
  </div>
</div>

<!-- best Pro Slider -->
<section class=" wow bounceInUp animated">
  <div class="best-pro slider-items-products container">
      <div class="new_title">
          <img src="images/icon2.png" alt="icon">
          <h2>Best Seller</h2>
      </div>

      <div id="best-seller" class="product-flexslider hidden-buttons">
          <div class="slider-items slider-width-col4 products-grid">
              @foreach($products as $product)
              <div class="item">
                  <div class="item-inner">
                      <div class="menu-7-img rel">
                          <!-- Image -->
                          <img class="img-fluid" src="{{url($product->images[0]->url)}}" alt="menu-image" />
          
                          <div class="item-box-hover">
                              <div class="box-inner">
                                  <!-- Price -->
                                  <div class="menu-7-price bg-coffee">
                                      <h5 class="h5-xs yellow-color">Rp {{number_format($product->price)}}</h5>
                                  </div>
                                  @if(isset(Auth::user()->id))
                                  <!-- Like Icon -->
                                  <div class="like-ico ico-20">
                                      <a href="#"><span class="flaticon-heart"></span></a>
                                  </div>
                                  @endif
                              </div>
                          </div>
                      </div>
          
                      <div class="item-info">
                          <div class="info-inner">
                              <!-- Title -->
                              <a class="level-top" href="{{ url('/product/detail/'.$product->id) }}">
                                  <h5 class="h5-sm">{{$product->name}}</h5>
                              </a>
                              <!-- Description -->
                              <p class="grey-color">{{$product->title_description_en}}</p>
                              <!-- Button -->
                              <a href="{{ url('add-to-cart/' . $product->id) }}" class="btn btn-sm btn-tra-grey yellow-hover">
                                  <span class="flaticon-shopping-bag"></span> <b>Add to Cart</b>
                              </a>
                          </div>
                      </div>
                  </div>
              </div>
              @endforeach
          </div>
      </div>
  </div>
</section>

<!-- best Pro Slider -->
<section class=" wow bounceInUp animated">
  <div class="best-pro slider-items-products container">

        @if(isset($home_video->url))
        <section class="river_video_area" 
            style="background-image:url('{{ url($home_video->url) }}');">
        @endif
        <section class="river_video_area">
            <div class="container">
                <div class="river_video_inner">
                    <h4>Live!</h4>
                    <!-- <h5>Much Better Experience With Video</h5> -->
                    <a class="big-icon-link lightbox-gallery-1 mfp-iframe" 
                        href="{{$home_video->link}}"><i class="fa fa-play"></i></a>
                </div>
            </div>
        </section>
  </div>
</section>

<!-- best Pro Slider -->
<section class=" wow bounceInUp animated">
  <div class="best-pro slider-items-products container">
      <div class="new_title">
          <img src="images/recipes.png" alt="icon" style="width: 50px;">
          <h2>Our Recipes</h2>
      </div>

      <div class="container">
          <div class="row">
              <!-- LightWidget WIDGET -->
              <script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script>
              <iframe src="https://cdn.lightwidget.com/widgets/596a46a8c9125ccea5c48a0ee3b16a81.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
          </div>
      </div>
  </div>
</section>

@endsection

@section('before_main_script')

    <script src="{{ asset('js/vendor.js') }}"></script>	
    <script type="text/javascript" src="{{ asset('js/slide/jquery-migrate-1.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/slide/all.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>

@endsection

@section('after_main_script')
    
    <script src="{{ asset('js/parallax.js') }}"></script>
    <script src="{{ asset('js/revslider.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mobile-menu.min.js') }}"></script>
    <script src="{{ asset('js/countdown.js') }}"></script>

    <script>
        jQuery(document).ready(function() {
            jQuery('#thm-rev-slider').show().revolution({
                dottedOverlay: 'none',
                delay: 5000,
                startwidth: 0,
                startheight: 710,

                hideThumbs: 200,
                thumbWidth: 200,
                thumbHeight: 50,
                thumbAmount: 2,

                navigationType: 'thumb',
                navigationArrows: 'solo',
                navigationStyle: 'round',

                touchenabled: 'on',
                onHoverStop: 'on',

                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,

                spinner: 'spinner0',
                keyboardNavigation: 'off',

                navigationHAlign: 'center',
                navigationVAlign: 'bottom',
                navigationHOffset: 0,
                navigationVOffset: 20,

                soloArrowLeftHalign: 'left',
                soloArrowLeftValign: 'center',
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,

                soloArrowRightHalign: 'right',
                soloArrowRightValign: 'center',
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,

                shadow: 0,
                fullWidth: 'on',
                fullScreen: 'on',

                stopLoop: 'off',
                stopAfterLoops: -1,
                stopAtSlide: -1,

                shuffle: 'off',

                autoHeight: 'on',
                forceFullWidth: 'off',
                fullScreenAlignForce: 'off',
                minFullScreenHeight: 0,
                hideNavDelayOnMobile: 1500,

                hideThumbsOnMobile: 'off',
                hideBulletsOnMobile: 'off',
                hideArrowsOnMobile: 'off',
                hideThumbsUnderResolution: 0,

                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                fullScreenOffsetContainer: ''
            });
        });
    </script>
    <script>
        function HideMe() {
            jQuery('.popup1').hide();
            jQuery('#fade').hide();
        }
    </script>
    <!-- Hot Deals Timer 1-->
    <script>
        var dthen1 = new Date("12/25/17 11:59:00 PM");
        start = "08/04/15 03:02:11 AM";
        start_date = Date.parse(start);
        var dnow1 = new Date(start_date);
        if (CountStepper > 0)
            ddiff = new Date((dnow1) - (dthen1));
        else
            ddiff = new Date((dthen1) - (dnow1));
        gsecs1 = Math.floor(ddiff.valueOf() / 1000);

        var iid1 = "countbox_1";
        CountBack_slider(gsecs1, "countbox_1", 1);
    </script>

    <script>
        jQuery(document).ready(function($) {

            // site preloader -- also uncomment the div in the header and the css style for #preloader
            $(window).load(function() {
                $('#preloader').fadeOut('slow', function() {
                    $(this).remove();
                });
            });

        });
    </script>

@endsection
