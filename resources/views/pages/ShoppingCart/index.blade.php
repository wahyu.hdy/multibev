@extends("Layouts.master")
@section('content')
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title">
                        <h2>Shopping Cart</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN Main Container -->

    <div class="main-container col1-layout wow bounceInUp animated">

        <div class="main">
            <div class="cart wow bounceInUp animated">

                <div class="table-responsive shopping-cart-tbl  container">
                    <form action="" method="post">
                        <input name="form_key" type="hidden" value="EPYwQxF6xoWcjLUr">
                        <fieldset>
                            <table id="shopping-cart-table" class="data-table cart-table table-striped">
                                <colgroup>
                                    <col width="1">
                                    <col>
                                    <col width="1">
                                    <col width="1">
                                    <col width="1">
                                    <col width="1">
                                    <col width="1">

                                </colgroup>
                                <thead>
                                    <tr class="first last">
                                        <th rowspan="1">&nbsp;</th>
                                        <th rowspan="1"><span class="nobr">Product Name</span></th>
                                        <th rowspan="1"></th>
                                        <th class="a-center" colspan="1"><span class="nobr">Unit
                                                Price</span></th>
                                        <th rowspan="1" class="a-center">Qty</th>
                                        <th class="a-center" colspan="1">Subtotal</th>
                                        <th rowspan="1" class="a-center">&nbsp;</th>
                                    </tr>
                                </thead>

                                @if($carts)
                                    @if(count($carts->details) > 0)
                                    <tfoot>
                                        <tr class="first last">
                                            <td colspan="50" class="a-right last">
                                                <button type="button" title="Continue Shopping" class="button btn-continue"
                                                    onClick=""><span><span>Continue Shopping</span></span></button>
                                                <button type="button" name="update_cart_action" value="empty_cart"
                                                    title="Clear Cart" class="button btn-empty"
                                                    onClick="confirmDelete('Are you sure delete all cart ?', '{{ url("delete-cart/" . $carts->id) }}')"
                                                    id="emptyCartButton"><span><span>Clear Cart</span></span></button>

                                            </td>
                                        </tr>
                                    </tfoot>
                                    @endif
                                @endif

                                <tbody>
                                    @if($carts)
                                        @if(count($carts->details) > 0)
                                            @foreach($carts->details as $cart)
                                                <tr class="first last odd">
                                                    <td class="image hidden-table"><a href="product-detail.html"
                                                        title="{{ $cart->product->name }}" class="product-image"><img
                                                            src="{{url($cart->product->images[0]->url)}}" width="75"
                                                            alt="{{ $cart->product->name }}"></a>
                                                    </td>
                                                    <td>
                                                        <h2 class="product-name">
                                                            <a href="product-detail.html">{{ $cart->product->name }}</a>
                                                        </h2>
                                                    </td>
                                                    <!-- <td class="a-center hidden-table">
                                                        <a href="#" class="edit-bnt" title="Edit item parameters"></a>
                                                    </td> -->
                                                    <td class="a-right hidden-table">
                                                        <span class="cart-price">
                                                            <span class="price">Rp. {{ number_format($cart->product->price) }}</span>
                                                        </span>
                                                    </td>
                                                    <td class="a-center movewishlist">
                                                        <input name="qty[]" value="{{ $cart->qty }}" title="Qty"
                                                            onchange="changeQty({{$carts->id}}, {{$cart->product->id}}, this)"
                                                            class="input-text qty">
                                                    </td>
                                                    <td class="a-right movewishlist">
                                                        <span class="cart-price">
                                                            <span class="price">Rp. <span id="price{{$carts->id}}">{{ number_format($cart->qty * $cart->product->price) }}</span></span>
                                                        </span>
                                                    </td>
                                                    <td class="a-center last">
                                                        <a href="#" 
                                                            onClick="confirmDelete('Are you sure delete this product ?', '{{url("delete-product-cart/" . $carts->id ."/". $cart->product_id)}}')"
                                                            title="Remove item" class="button remove-item"><span><span>Remove
                                                            item</span></span></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr class="first last odd">
                                                <td class="a-center hidden-table" colspan="7">
                                                    <p class="text-center">Cart is empty</p>
                                                </td>
                                            </tr>
                                        @endif
                                    @else
                                        <tr class="first last odd">
                                            <td class="a-center hidden-table" colspan="7">
                                                <p class="text-center">Cart is empty</p>
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </fieldset>
                    </form>
                </div>

                <!-- BEGIN CART COLLATERALS -->

                @if($carts)
                    @if(count($carts->details) > 0)
                    <div class="cart-collaterals container">
                        <!-- BEGIN COL2 SEL COL 1 -->
                        <div class="row">
                            <!-- BEGIN TOTALS COL 2 -->
                            <div class="col-sm-4">
                                <div class="shipping">
                                    <h3>Estimate Shipping and Tax</h3>
                                    <div class="shipping-form">
                                        <form action="" method="post" id="shipping-zip-form">
                                            <p>Enter your destination to get a shipping estimate.</p>
                                            <ul class="form-list">
                                                <li>
                                                    <label for="country" class="required"><em>*</em>Country</label>
                                                    <div class="input-box">
                                                        <select name="country_id" id="country" class="validate-select"
                                                            title="Country">
                                                            <option value=""> </option>
                                                            <option value="AF">Afghanistan</option>
                                                        </select>
                                                    </div>
                                                </li>
                                                <li>
                                                    <label for="region_id">State/Province</label>
                                                    <div class="input-box">
                                                        <select id="region_id" name="region_id" title="State/Province"
                                                            defaultvalue="" class="required-entry validate-select">
                                                            <option value="">Please select region, state or province</option>
                                                            <option value="1" title="Alabama">Alabama</option>
                                                        </select>

                                                        <input type="text" id="region" name="region" value=""
                                                            title="State/Province" class="input-text required-entry"
                                                            style="display: none;">
                                                    </div>
                                                </li>
                                                <li>
                                                    <label for="postcode">Zip/Postal Code</label>
                                                    <div class="input-box">
                                                        <input class="input-text validate-postcode" type="text" id="postcode"
                                                            name="estimate_postcode" value="">
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="buttons-set11">
                                                <button type="button" title="Get a Quote"
                                                    onClick="coShippingMethodForm.submit()" class="button get-quote"><span>Get
                                                        a Quote</span></button>
                                            </div>
                                            <!--buttons-set11-->
                                        </form>
                                    </div>
                                </div>

                            </div>

                            <div class="col-sm-4">

                                <div class="discount">
                                    <h3>Discount Codes</h3>
                                    <form id="discount-coupon-form" action="" method="post">
                                        <label for="coupon_code">Enter your coupon code if you have one.</label>
                                        <input type="hidden" name="remove" id="remove-coupone" value="0">
                                        <input class="input-text fullwidth" type="text" id="coupon_code" name="coupon_code"
                                            value="">
                                        <button type="button" title="Apply Coupon" class="button coupon "
                                            onClick="discountForm.submit(false)" value="Apply Coupon"><span>Apply
                                                Coupon</span></button>
                                    </form>
                                </div>
                                <!--discount-->
                            </div>
                            <!--col-sm-4-->

                            <div class="col-sm-4">
                                <div class="totals">
                                    <h3>Shopping Cart Total</h3>
                                    <div class="inner">

                                        <table id="shopping-cart-totals-table" class="table shopping-cart-table-total">
                                            <colgroup>
                                                <col>
                                                <col width="1">
                                            </colgroup>
                                            <tfoot>
                                                <tr>
                                                    <td style="" class="a-left" colspan="1">
                                                        <strong>Grand Total</strong>
                                                    </td>
                                                    <td style="" class="a-right">
                                                        <strong><span class="price">Rp. <span></span></span></strong>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <tr>
                                                    <td style="" class="a-left" colspan="1">
                                                        Subtotal </td>
                                                    <td style="" class="a-right">
                                                        <span class="price">Rp. <span></span></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <ul class="checkout">
                                            <li>
                                                <button type="button" title="Proceed to Checkout"
                                                    class="button btn-proceed-checkout" onClick=""><span>Proceed to
                                                        Checkout</span></button>
                                            </li><br>
                                            <li><a href="multiple-addresses.html"
                                                    title="Checkout with Multiple Addresses">Checkout with Multiple
                                                    Addresses</a>
                                            </li><br>
                                        </ul>
                                    </div>
                                    <!--inner-->
                                </div>
                                <!--totals-->
                            </div>
                            <!--col-sm-4-->
                        </div>
                        <!--cart-collaterals-->
                        @endif
                    @endif

                </div>
            </div>
            <!--cart-->

        </div>
        <!--main-container-->

    </div>
    <!--col1-layout-->
@endsection

@section('after_main_script')
    <script src="{{ asset('js/parallax.js') }}"></script>
    <script src="{{ asset('js/revslider.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
    <script src="{{ asset('js/jquery.flexslider.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mobile-menu.min.js') }}"></script>

    <script>
        function confirmDelete(message,url) {
            jQuery.confirm({
                title: 'Delete product cart',
                content: `<div style="text-align:left;">${message}</div>`,
                type:'blue',
                buttons: {
                    Ya: {
                        text: '<i class="fa fa-check-circle"></i> Yes',
                        btnClass: 'btn-blue',
                        action: function () {
                            window.location.href = url;
                        }
                    },
                    Tidak: {
                        text: '<i class="fa fa-times-circle"></i> No',
                        btnClass:'btn-red'
                    }
                }
            });
        }

        function changeQty(cartId, productId, qty) {
            jQuery.ajax({
                url: "{{url('update-qty-cart')}}",
                type: 'POST',
                dataType: 'json',
                data: {
                    cart_id: cartId,
                    product_id: productId,
                    qty: qty.value,
                   _token: '{!! csrf_token() !!}',
                },
                success: function(res) {
                    if (res.status) {
                        jQuery(`#price${cartId}`).empty().append(res.product_price);
                    }
                }
            });
        }
    </script>
@endsection
