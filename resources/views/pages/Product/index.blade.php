@extends("Layouts.master")
@section('content')

    <div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
       <div class="page-title"><h2>Product List</h2></div>
        </div>
        <!--col-xs-12-->
      </div>
      <!--row-->
    </div>
    <!--container-->
  </div>    	
<!--breadcrumbs--> 
<!-- BEGIN Main Container col2-left -->
<section class="main-container col2-left-layout bounceInUp animated"> 
  <!-- For version 1, 2, 3, 8 --> 
  <!-- For version 1, 2, 3 -->
  <div class="container">
    <div class="row">
      <div class="col-main col-sm-9 col-sm-push-3 product-grid">
      <div class="pro-coloumn">
      <div class="category-description std">
            <div class="slider-items-products">
              <div id="category-desc-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 
                
                  @if($product_slides)
                    @foreach($product_slides as $product_slide)
                    <div class="item"> <a href="#"><img alt="" src="{{ url($product_slide->url) }}"></a>
                      <div class="cat-img-title cat-bg cat-box">
                        <div class="small-tag">{{$product_slide->title_en}}</div>
                        <h2 class="cat-heading">{{$product_slide->content_en}}</h2>
                        <!-- <p>GET 40% OFF &sdot; Free Delivery </p> -->
                      </div>
                    </div>
                    @endforeach
                  @endif

                </div>
              </div>
            </div>
          </div>
      
        <article>
          <div class="toolbar">
            

            <div class="sort-by">
              <label class="left">Order: </label>
              <ul>
                <li>              
                  <a href="{{ Request::fullUrlWithQuery(['sort_order' => $order]) }}">{{ucfirst($sort_order)}}<span class="right-arrow"></span></a>  
                  <ul>
                    @if($sort_order != 'asc')
                      <li><a href="{{ Request::fullUrlWithQuery(['sort_order' => 'asc']) }}">Asc</a></li>
                    @endif
                    @if($sort_order != 'desc')
                      <li><a href="{{ Request::fullUrlWithQuery(['sort_order' => 'desc']) }}">Desc</a></li>
                    @endif
                  </ul>
                </li>
              </ul>
            </div>

            <div class="sort-by">
              <label class="left">Sort By: </label>
              <ul>
                <li>              
                  <a href="{{ Request::fullUrlWithQuery(['sort_field' => $sort_field]) }}">{{ucfirst($sort_field)}}<span class="right-arrow"></span></a>  
                  <ul>
                    @if($sort_field != 'name')
                      <li><a href="{{ Request::fullUrlWithQuery(['sort_field' => 'name']) }}">Name</a></li>
                    @endif
                    @if($sort_field != 'price')
                      <li><a href="{{ Request::fullUrlWithQuery(['sort_field' => 'price']) }}">Price</a></li>
                    @endif
                  </ul>
                </li>
              </ul>
            </div>


            <div class="pager">
              <div class="limiter">
                <label>View: </label>
                <ul>
                  <li>
                      <a href="{{ Request::fullUrlWithQuery(['per_page' => $per_page]) }}">{{$per_page}}<span class="right-arrow"></span></a>
                    <ul>
                      @if($per_page != 6)
                        <li><a href="{{ Request::fullUrlWithQuery(['per_page' => 6]) }}">6</a></li>
                      @endif
                      @if($per_page != 20)
                        <li><a href="{{ Request::fullUrlWithQuery(['per_page' => 20]) }}">20</a></li>
                      @endif
                      @if($per_page != 30)
                        <li><a href="{{ Request::fullUrlWithQuery(['per_page' => 30]) }}">30</a></li>
                      @endif
                    </ul>
                  </li>
                </ul>
              </div>
              
            </div>
          </div>
          <div class="category-products">
            <ul class="products-grid">

              @foreach($products as $product)
              <li class="item col-lg-4 col-md-3 col-sm-4 col-xs-6">
                <div class="item-inner">
                              <!-- IMAGE -->
                  <div class="menu-7-img rel">
      
                    <!-- Image -->
                    <img class="img-fluid" src="{{ url($product->images[0]->url) }}" alt="menu-image" />

                    <!-- Price -->
                    <div class="menu-7-price bg-coffee">
                      <h5 class="h5-xs yellow-color">Rp {{number_format($product->price)}}</h5>
                    </div>

                    @if(isset(Auth::user()->id))
                    <!-- Like Icon -->
                    <div class="like-ico ico-20">
                      <a href="#"><span class="flaticon-heart"></span></a>
                    </div>
                    @endif

                  </div>
                   <!-- TEXT -->
                   <div class="menu-7-txt rel">

                    <!-- Title -->
                    <a class="level-top" href="{{ url('/product/detail/'.$product->id) }}">
                      <h5 class="h5-sm">{{$product->name}}</h5>
                    </a>
                    
                    <!-- Description -->
                    <p class="grey-color">{{$product->name}}</p>

                    <!-- Button -->
                    <a href="{{ url('add-to-cart/' . $product->id) }}" class="btn btn-sm btn-tra-grey yellow-hover">
                      <span class="flaticon-shopping-bag"></span> <b>Add to Cart</b>
                    </a>

                  </div>
                </li>
                @endforeach

            </ul>
          </div>
          <div class="toolbar bottom">
              <div class="display-product-option">
                <div class="pages">
                  <!-- <label>Page:</label> -->

                  {{$products->links()}}
                  <!-- <ul class="pagination">
                    <li><a href="#">«</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">»</a></li>
                  </ul> -->
                </div>
              </div>
            </div>
        </article>
        </div>
        <!--	///*///======    End article  ========= //*/// --> 
      </div>
      <aside class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9 wow bounceInUp animated"> 
        <!-- BEGIN SIDE-NAV-CATEGORY -->
        <div class="side-nav-categories">
          <div class="block-title"> Categories </div>
          <!--block-title--> 
          <!-- BEGIN BOX-CATEGORY -->
          <div class="box-content box-category">
            <ul>
              @foreach($categories as $categoy)
                @if(count($categoy->childs) == 0)
                  <li> <a href="{{ url('/product/'.$categoy->id) }}">{{$categoy->title_en}}</a> </li>
                @else
                  <li> 
                      <a href="{{ url('/product/'.$categoy->id) }}">{{$categoy->title_en}}</a> 
                      <span class="subDropdown minus"></span>
                      <ul class="level1">
                        @foreach($categoy->childs as $child)
                        <li> <a href="{{ url('/product/'.$child->id) }}"><span>{{$child->title_en}}</span></a> </li>
                        @endforeach
                      </ul>
                  </li>
                @endif
              @endforeach 
            </ul>
          </div>
          <!--box-content box-category--> 
        </div>
        <!--side-nav-categories-->
       
        <!-- <div class="custom-slider">
          <div>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li class="active" data-target="#carousel-example-generic" data-slide-to="0"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
              </ol>
              <div class="carousel-inner">
                <div class="item active"><img src="{{ asset('images/slide2.jpg') }}" alt="slide3">
                  <div class="carousel-caption">
                  <h4>Fruit Shop</h4>
                    <h3><a title=" Sample Product" href="product-detail.html">Up to 70% Off</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <a class="link" href="#">Buy Now</a></div>
                </div>
                <div class="item"><img src="{{ asset('images/slide3.jpg') }}" alt="slide1">
                  <div class="carousel-caption">
                   <h4>Black Grapes</h4>
                    <h3><a title=" Sample Product" href="product-detail.html">Mega Sale</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                     <a class="link" href="#">Buy Now</a>
                  </div>
                </div>
                <div class="item"><img src="{{ asset('images/slide1.jpg') }}" alt="slide2">
                  <div class="carousel-caption">
                  <h4>Food Farm</h4>
                    <h3><a title=" Sample Product" href="product-detail.html">Up to 50% Off</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                     <a class="link" href="#">Buy Now</a>
                  </div>
                </div>
              </div>
              <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> <span class="sr-only">Next</span> </a></div>
          </div>
        </div> -->
       
        <!-- <div class="block block-list block-cart">
          <div class="block-title"> My Cart </div>
          <div class="block-content">
            <div class="summary">
              <p class="amount">There is <a href="#">1 item</a> in your cart.</p>
              <p class="subtotal"> <span class="label">Cart Subtotal:</span> <span class="price">$299.00</span> </p>
            </div>
            <div class="ajax-checkout">
              <button type="button" title="Checkout" class="button button-checkout" onClick="#"> <span>Checkout</span> </button>
            </div>
            <p class="block-subtitle">Recently added item(s)</p>
            <ul id="cart-sidebar1" class="mini-products-list">
              <li class="item">
                <div class="item-inner"> <a href="#" class="product-image"><img src="{{ asset('products-images/p1.jpg') }}" width="80" alt="product"></a>
                  <div class="product-details">
                    <div class="access"> <a href="#" class="btn-remove1">Remove</a> 
                    <a href="" title="Edit item" class="btn-edit">
                    <i class="icon-pencil"></i><span class="hidden">Edit item</span></a> </div>
                    
                    <strong>1</strong> x <span class="price">$299.00</span>
                    <p class="product-name"><a href="#">Fresh Organic Mustard Leaves </a></p>
                  </div>
                </div>
              </li>
              <li class="item  last1">
                <div class="item-inner"> <a href="#" class="product-image"><img src="{{ asset('products-images/p2.jpg') }}" width="80" alt="product"></a>
                  <div class="product-details">
                    <div class="access"> <a href="#" class="btn-remove1">Remove</a> 
                    <a href="" title="Edit item" class="btn-edit">
                    <i class="icon-pencil"></i><span class="hidden">Edit item</span></a> </div>
                    
                    <strong>1</strong> x <span class="price">$299.00</span>
                    <p class="product-name"><a href="#">Fresh Organic Mustard Leaves </a></p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div> -->

      </aside>
      <!--col-right sidebar--> 
    </div>
    <!--row--> 
  </div>
  <!--container--> 
</section>
<!--main-container col2-left-layout--> 

@endsection

@section('after_main_script')

  <script src="{{ asset('js/common.js') }}"></script>
  <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
  <script src="{{ asset('js/jquery.flexslider.js') }}"></script>
  <script src="{{ asset('js/jquery.mobile-menu.min.js') }}"></script>

@endsection