@extends("Layouts.master")
@section('content')

<div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
       <div class="page-title"><h2>Product Detail</h2></div>
        </div>
        <!--col-xs-12-->
      </div>
      <!--row-->
    </div>
    <!--container-->
  </div>    	
<!--breadcrumbs--> 
  <!-- BEGIN Main Container -->
  <div class="main-container col1-layout wow bounceInUp animated">
    <div class="main">
      <div class="col-main">
        <!-- Endif Next Previous Product -->
        <div class="product-view wow bounceInUp animated" itemscope="" itemtype="http://schema.org/Product" itemid="#product_base">
          <div id="messages_product_view"></div>
          <!--product-next-prev-->
          <div class="product-essential container">
            <div class="row">
              
              <form action="{{ url('add-to-cart/' . $product->id) }}" method="get" id="product_addtocart_form">
                <!--End For version 1, 2, 6 -->
                <!-- For version 3 -->
                <div class="product-img-box col-lg-5 col-sm-5 col-xs-12 pt-100">
                    <div class="product-image">
                      
                      <div class="product-full"> 
                        <img id="product-zoom" src="{{ url($product->images[0]->url) }}" 
                          data-zoom-image="{{ url($product->images[0]->url) }}" alt="product-image"/> 
                      </div>

                      <div class="more-views">
                        <div class="slider-items-products">
                          <div id="gallery_01" class="product-flexslider hidden-buttons product-img-thumb">
                            <div class="slider-items slider-width-col4 block-content">
                              @foreach($product->images as $key => $image)
                              <div class="more-views-items"> 
                                <a href="#" data-image="{{ url($image->url) }}" 
                                  data-zoom-image="{{ url($image->url) }}"> 
                                  <img id="product-zoom{{$key}}"  src="{{ url($image->url) }}" alt="product-image"/> 
                                </a>
                              </div>
                              @endforeach
                              
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- end: more-images --> 
                  </div>
                <!--End For version 1,2,6-->
                <!-- For version 3 -->
                <div class="product-shop col-lg- col-sm-7 col-xs-12">
                  <div class="product-description">

                    <!-- TITLE -->
                    <div class="project-title">

                      <!-- Title -->
                      <h2 class="h2-lg">{{$product->name}}</h2>

                      <!-- Price -->
                      <div class="project-price">
                        <h4 class="h4-xl yellow-color">
                          <!-- <span class="old-price">Rp 66.500</span> Rp 55.000 -->
                          Rp. {{number_format($product->price)}}
                        </h4>
                      </div>

                    </div>

                    <!-- TEXT -->
                    <div class="product-txt">

                      <!-- Text -->
                      <p class="p-md grey-color">
                        {!! $product->description_en !!}
                      </p>

                      <!-- Product Data -->
                      <!-- <div class="product-info">
                        <p>Volume of syrup : <span> 1000 ml</span></p>
                        <p>Unique taste : <span> Yet versatile, easily matches well with other flavors</span></p>
                        <p>Certifcation : <span> Halal MUI & BPOM</span></p>
                        <p>Composition : <span> Sugar, Fructoce, Water, Fruit Consentrate, Grape flavour, Pengasam, Pengawet, Food Coloring (E112, E133)</span></p>
                      </div> -->

                      <input class="qty" type="number" name="qty" min="1" max="20" value="1">

                      <!-- Add To Cart -->
                      <div class="add-to-cart-btn bg-yellow ico-20 text-center">
                        <button type="submit" 
                          style="background-color: transparent; background-repeat: no-repeat; border: none; cursor: pointer; overflow: hidden;outline: none;">
                            <span class="flaticon-shopping-bag"></span> Add to Cart</button>
                      </div>

                    </div>	<!-- END TEXT-->

                  </div>
                </div>	<!-- END PRODUCT DISCRIPTION -->
                  </div>
                <!--product-shop-->
                <!--Detail page static block for version 3-->
              </form>
            </div>
          </div>
          <!--product-essential-->
      
          <!--product-collateral-->
          <div class="box-additional">
            <!-- BEGIN RELATED PRODUCTS -->
            <div class="related-pro container">
              <div class="slider-items-products">
                <div class="new_title center">
                  <h2>Related Products</h2>
                </div>
                <div id="related-slider" class="product-flexslider hidden-buttons">
                  <div class="slider-items slider-width-col4 products-grid">

                    @foreach($products as $product)
                    <!-- Item -->
                    <div class="item">
                      <div class="item-inner">
                        
                        <div class="menu-7-item">

                          <!-- IMAGE -->
                          <div class="menu-7-img rel">
              
                            <!-- Image -->
                            <img class="img-fluid" src="{{ url($product->images[0]->url) }}" alt="menu-image" />

                            <!-- Price -->
                            <div class="menu-7-price bg-coffee">
                              <h5 class="h5-xs yellow-color">Rp. {{number_format($product->price)}}</h5>
                            </div>

                            <!-- Like Icon -->
                            <div class="like-ico ico-20">
                              <a href="#"><span class="flaticon-heart"></span></a>
                            </div>

                          </div>

                          <!-- TEXT -->
                          <div class="menu-7-txt rel">

                            <!-- Title -->
                            <a class="level-top" href="{{ url('/product/detail/'.$product->id) }}">
                              <h5 class="h5-sm">{{$product->name}}</h5>
                            </a>

                            <!-- Description -->
                            <!-- <p class="grey-color">{{!!$product->description_en!!}}</p> -->

                            <!-- Button -->
                            <a href="{{$product->marketplacelink}}" target="_blank" class="btn btn-sm btn-tra-grey yellow-hover">
                              <span class="flaticon-shopping-bag"></span> <b>Add to Cart</b>
                            </a>

                          </div>

                        </div>

                      </div>
                    </div>
                    <!-- End Item --> 
                    @endforeach

              </div>
            </div>
            <!-- End Item --> 
        </div>
                </div>
              </div>
            </div>
            <!-- end related product -->
            
          </div>
          <!-- end related product -->
        </div>
        <!--box-additional-->
        <!--product-view-->
      </div>
    </div>
    <!--col-main-->
  </div>
  <!--main-container-->
</div>

@endsection

@section('after_main_script')

  <script src="{{ asset('js/common.js') }}"></script>
  <script src="{{ asset('js/jquery.flexslider.js') }}"></script>
  <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('js/cloud-zoom.js') }}"></script>
  <script src="{{ asset('js/jquery.mobile-menu.min.js') }}"></script>

@endsection
