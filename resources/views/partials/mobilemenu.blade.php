<div id="mobile-menu">
  <ul>
  <li>
      <div class="mm-search">
        <form action="{{url('products')}}" method="get" class="navbar-form" role="search">
          <div class="input-group">
            <input type="text" name="search" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
             <div class="input-group-btn">
              <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> </button>
            </div>
          </div>
        </form>
      </div>
    </li>
    <li>
      <div class="home"><a href="{{ url('/about') }}">About</a> </div>
    </li>
    <li><a href="{{ url('/ournetwork') }}">Our Network</a></li>
    <li><a href="{{ url('/recipes') }}">News & Updates</a></li>
    <li><a href="#">Shop</a>
      <ul>
        @foreach($categories_menu as $category_menu)
        <li><a href="{{ url('/product/'.$category_menu->id) }}">{{$category_menu->title_en}}</a></li>
        @endforeach
      </ul>
    </li>
    <li><a href="#">Contact</a></li>
    <li><a href="{{ url('/dashboard') }}">My Account</a></li>
    <li><a href="{{ url('/wishlist') }}">Wishlist</a></li>
    <li><a href="{{ url('/checkout') }}">Checkout</a></li>
    <li><a href="{{ url('/blog') }}">Blog</a></li>
    
    @if(isset(Auth::user()->id))
    <li><a href="{{ url('/logout') }}">Logout</a></li>
    @else
    <li><a href="{{ url('/login') }}">Login</a></li>
    @endif
    
  </ul>
  {{-- <div class="top-links">
    <ul class="links">
      <li><a title="My Account" href="login.html">My Account</a> </li>
      <li><a title="Wishlist" href="wishlist.html">Wishlist</a> </li>
      <li><a title="Checkout" href="checkout.html">Checkout</a> </li>
      <li><a title="Blog" href="blog.html">Blog</a> </li>
      <li class="last"><a title="Login" href="login.html">Login</a> </li>
    </ul>
  </div> --}}
</div>