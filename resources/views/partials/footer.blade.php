<footer> 
  <!-- BEGIN INFORMATIVE FOOTER -->
  <div class="footer-inner">
    <div class="newsletter-row">
      <div class="container">
        <div class="row"> 
          
          <!-- Footer Newsletter -->
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col1">
            <div class="newsletter-wrap">
              <h5>Newsletter</h5>
              <h4>Get discount 30% off</h4>
              
              @if($errors->has('email'))
                <p style="color: red">{{ $errors->first('email') }}</p>
              @endif

              @if(session()->has('message'))
                <p style="color: white">{{ session()->get('message') }}</p>
              @endif

              <form action="{{ url('newsletter-store') }}" method="post" id="newsletter-validate-detail1">
                <div id="container_form_news">
                  <div id="container_form_news2">
                    {{ csrf_field() }}
                    <input type="text" name="email" id="newsletter1" title="Sign up for our newsletter" class="input-text required-entry validate-email" placeholder="Enter your email address">
                    <button type="submit" title="Subscribe" class="button subscribe"><span>Subscribe</span></button>
                  </div>
                  <!--container_form_news2--> 
                </div>
                <!--container_form_news-->
              </form>
            </div>
            <!--newsletter-wrap--> 
          </div>
        </div>
      </div>
      <!--footer-column-last--> 
    </div>
    <div class="footer-middle">
      <div class="container">
        <div class="row">
          @foreach(array_keys($menus) as $key)
          <div class="col-md-3 col-sm-6">
            <div class="footer-column">
              <h4>{{$key}}</h4>
              <ul class="links">
                @foreach($menus[$key] as $menu_item)
                <li><a href="{{$menu_item->url}}" title="{{$menu_item->name_en}}">{{$menu_item->name_en}}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
    
    <!--container--> 
  </div>
  <!--footer-inner--> 
  
  <!--footer-middle-->
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-4">
          <div class="social">
            <!-- <ul>
              <li class="fb"><a href="{{$configs[4]}}"></a></li>
              <li class="tw"><a href="{{$configs[6]}}"></a></li>
            </ul> -->
          </div>
        </div>
        <div class="col-sm-4 col-xs-12 coppyright"> © {{ date('Y') }} Multibev. All Rights Reserved. </div>
        <div class="col-xs-12 col-sm-4">
          <div class="payment-accept"> 
            @foreach($payment_methods as $payment_method)
            <img src="{{ url($payment_method) }}" alt="{{$payment_method}}"> 
            @endforeach
          </div>
      </div>
    </div>
  </div>
  <!--footer-bottom--> 
  <!-- BEGIN SIMPLE FOOTER --> 
</footer>