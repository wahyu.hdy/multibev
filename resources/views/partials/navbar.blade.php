<header>
  <div class="header-banner">
    <div class="assetBlock">
      <div class="col-12 text-center">
          <div class="social-icons">
            <a href="{{$configs[4]}}" target="_blank"><span class="icon-facebook"></span></a>
            <a href="{{$configs[5]}}" target="_blank"><span class="icon-instagram"></span></a>
            <a href="{{$configs[6]}}" target="_blank"><span class="icon-twitter"></span></a>
          </div>
        </div>
    </div>
  </div>
  <div id="header">
    <div class="container">
      <div class="header-container row">
        <div class="logo"> <a href="{{ url('/') }}" title="index">
          <div><img src="{{ url($configs[7]) }}" alt="logo" style="max-width: 184px;"></div>
          </a> </div>
        <div class="fl-nav-menu">
          <nav >
            <div class="mm-toggle-wrap">
              <div class="mm-toggle"><i class="icon-align-justify"></i><span class="mm-label">Menu</span> </div>
            </div>
            <div class="nav-inner"> 
              <!-- BEGIN NAV -->
              <ul id="nav" class="hidden-xs">
                <li style="margin-left:150px;"> <a class="level-top" href="{{ url('/about') }}"><span>About</span></a></li>
                
                <li class="mega-menu"> <a class="level-top" href="{{ url('/ournetwork') }}"><span>Our Network</span></a> </li>
                <li class="mega-menu"> <a class="level-top" href="{{ url('/recipes') }}"><span>News & Updates</span></a> </li>
                <li class="fl-custom-tabmenulink mega-menu"> <a href="#" class="level-top"> <span>Shop</span> </a>
                  <div class="level0-wrapper fl-custom-tabmenu" style="left: 0px; display: none;">
                    <div class="container">
                      <div class="header-nav-dropdown-wrapper clearer">
                        @foreach($categories_menu as $category_menu)
                        <div class="grid12-3">
                          <a class="level-top" href="{{ url('/product/'.$category_menu->id) }}">
                            <div><img src="{{ isset($category_menu->cover_image) ? url($category_menu->cover_image) : '' }}" alt="custom-image"></div>
                            <h4 class="heading">{{$category_menu->title_en}}</h4>
                            <p>{{$category_menu->description_en}}</p>
                          </a>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </li>
                
                
                <li class="mega-menu"> <a class="level-top" href="#"><span>Contact</span></a> </li>
                
              </ul>
              <!--nav--> 
            </div>
          </nav>
        </div>
        
        <!--row-->
        
        <div class="fl-header-right">
          <div class="fl-links">
            <div class="no-js"> <a title="Company" class="clicker"></a>
              <div class="fl-nav-links">
                <ul class="links">
                  <li><a href="{{ url('/dashboard') }}" title="My Account">My Account</a></li>
                  <li><a href="{{ url('/wishlist') }}" title="Wishlist">Wishlist</a></li>
                  <li><a href="{{ url('/checkout') }}" title="Checkout">Checkout</a></li>
                  <li><a href="{{ url('/blog') }}" title="Blog"><span>Blog</span></a></li>
                  @if(isset(Auth::user()->id))
                  <li class="last"><a href="{{ url('/logout') }}" title="Logout"><span>Logout</span></a></li>
                  @else
                  <li class="last"><a href="{{ url('/login') }}" title="Login"><span>Login</span></a></li>
                  @endif
                </ul>
              </div>
            </div>
          </div>
          <div class="fl-cart-contain">
            <div class="mini-cart">
              <div class="basket"> 
                <a href="{{ url('/shopping-cart') }}">
                  @if(isset(Auth::user()->id))
                  <span id="cartTotal"></span>
                  @endif
                </a> 
              </div>
              <!--fl-mini-cart-content--> 
            </div>
          </div>
          <!--mini-cart-->
          <div class="fl-search">
            <div class="no-js"> 
              <a title="Company" class="clicker"></a>
              <div class="fl-nav-search">
                <form action="{{url('products')}}" method="get" class="navbar-form" role="search">
                  <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search">
                    <span class="input-group-btn">
                      <button class="ori-btn ori-btn-default" type="submit"><i style="padding:3px" class="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>