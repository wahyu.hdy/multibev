
<header>
  <div id="header">
    <div class="container">
      <div class="header-container row">
        <div class="logo"> <a href="{{ url('/') }}" title="index">
          <div><img src="{{ asset('images/logo/multibev-orange.png') }}" alt="logo" style="max-width: 184px;"></div>
          </a> </div>
        <div class="fl-nav-menu">
          <nav >
            <div class="mm-toggle-wrap">
              <div class="mm-toggle"><i class="icon-align-justify"></i><span class="mm-label">Menu</span> </div>
            </div>
            <div class="nav-inner"> 
              <!-- BEGIN NAV -->
              <ul id="nav" class="hidden-xs">
                <li class="mega-menu"> <a class="level-top" href="{{ url('/about') }}"><span>About</span></a> </li>
                <li class="mega-menu"> <a class="level-top" href="{{ url('/recipes') }}"><span>Our network</span></a> </li>
                <li class="mega-menu"> <a class="level-top" href="{{ url('/recipes') }}"><span>News & Update</span></a> </li>
                <li class="fl-custom-tabmenulink mega-menu"> <a href="{{ url('#') }}" class="level-top"> <span>Shop</span> </a>
                  <div class="level0-wrapper fl-custom-tabmenu">
                    <div class="container">
                      <div class="header-nav-dropdown-wrapper clearer">
                        <div class="grid12-3">
                          <a class="level-top" href="{{ url('group/1') }}">
                            <div><img src="{{ asset('images/custom-img1.jpg') }}" alt="custom-image"></div>
                            <h4 class="heading">Pouch</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                          </a>
                        </div>
                        <div class="grid12-3">
                          <a class="level-top" href="{{ url('group/2') }}">
                            <div><img src="{{ asset('images/custom-img2.jpg') }}" alt="custom-image"></div>
                            <h4 class="heading">Powder</h4>
                            <p>Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                          </a>
                        </div>
                        <div class="grid12-3">
                          <a class="level-top" href="{{ url('group/3') }}">
                            <div><img src="{{ asset('images/custom-img3.jpg') }}" alt="custom-image"></div>
                            <h4 class="heading">Syrup</h4>
                            <p>Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                          </a>
                        </div>
                        <div class="grid12-3">
                          <a class="level-top" href="{{ url('group/4') }}">
                            <div><img src="{{ asset('images/custom-img4.jpg') }}" alt="custom-image"></div>
                            <h4 class="heading">Puree</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                          </a>
                        </div>
                        <div class="grid12-3">
                          <a class="level-top" href="{{ url('group/5') }}">
                            <div><img src="{{ asset('images/custom-img4.jpg') }}" alt="custom-image"></div>
                            <h4 class="heading">Tea Leaves</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                          </a>
                        </div>
                        <div class="grid12-3">
                          <a class="level-top" href="{{ url('group/6') }}">
                            <div><img src="{{ asset('images/custom-img4.jpg') }}" alt="custom-image"></div>
                            <h4 class="heading">Bag-in-Box</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li class="mega-menu"> <a class="level-top" href="{{ url('/contact') }}"><span>Contact</span></a> </li>
                
              </ul>
              <!--nav--> 
            </div>
          </nav>
        </div>
        
        <!--row-->
        
        <div class="fl-header-right">
          <div class="fl-links">
            <div class="no-js"> <a title="Company" class="clicker"></a>
              <div class="fl-nav-links">
                <ul class="links">
                  <li><a href="{{ url('dashboard.html') }}" title="My Account">My Account</a></li>
                  <li><a href="{{ url('wishlist.html') }}" title="Wishlist">Wishlist</a></li>
                  <li><a href="{{ url('checkout.html') }}" title="Checkout">Checkout</a></li>
                  <li><a href="{{ url('blog.html') }}" title="Blog"><span>Blog</span></a></li>
                  <li class="last"><a href="{{ url('login.html') }}" title="Login"><span>Login</span></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="fl-cart-contain">
            <div class="mini-cart">
              <div class="basket"> <a href="{{ url('shopping-cart.html') }}"><span> 2 </span></a> </div>
              <div class="fl-mini-cart-content" style="display: none;">
                <div class="block-subtitle">
                  <div class="top-subtotal">2 items, <span class="price">$259.99</span> </div>
                  <!--top-subtotal--> 
                  <!--pull-right--> 
                </div>
                <!--block-subtitle-->
                <ul class="mini-products-list" id="cart-sidebar">
                  <li class="item first">
                    <div class="item-inner"><a class="product-image" title="timi &amp; leslie Sophia Diaper Bag, Lemon Yellow/Shadow White" href="{{ url('#l') }}"><img alt="timi &amp; leslie Sophia Diaper Bag, Lemon Yellow/Shadow White" src="{{ asset('products-images/p4.jpg') }}"></a>
                      <div class="product-details">
                        <div class="access"><a class="btn-remove1" title="Remove This Item" href="{{ url('#') }}">Remove</a> <a class="btn-edit" title="Edit item" href="{{ url('#') }}"><i class="icon-pencil"></i><span class="hidden">Edit item</span></a> </div>
                        <!--access--> 
                        <strong>1</strong> x <span class="price">$179.99</span>
                        <p class="product-name"><a href="{{ url('product-detail.html') }}">Fresh Organic Mustard Leaves</a></p>
                      </div>
                    </div>
                  </li>
                  <li class="item last">
                    <div class="item-inner"><a class="product-image" title="JP Lizzy Satchel Designer Diaper Bag - Slate Citron" href="{{ url('#') }}"><img alt="JP Lizzy Satchel Designer Diaper Bag - Slate Citron" src="{{ asset('products-images/p3.jpg') }}"></a>
                      <div class="product-details">
                        <div class="access"><a class="btn-remove1" title="Remove This Item" href="{{ url('#') }}">Remove</a> <a class="btn-edit" title="Edit item" href="{{ url('#') }}"><i class="icon-pencil"></i><span class="hidden">Edit item</span></a> </div>
                        <!--access--> 
                        <strong>1</strong> x <span class="price">$80.00</span>
                        <p class="product-name"><a href="{{ url('product-detail.html') }}">Fresh Organic Mustard Leaves</a></p>
                      </div>
                    </div>
                  </li>
                </ul>
                <div class="actions">
                  <button class="btn-checkout" title="Checkout" type="button" onClick="window.location=checkout.html') }}"><span>Checkout</span></button>
                </div>
                <!--actions--> 
              </div>
              <!--fl-mini-cart-content--> 
            </div>
          </div>
          <!--mini-cart-->
          <div class="fl-search">
            <div class="no-js"> 
              <a title="Company" class="clicker"></a>
              <div class="fl-nav-search">
                <form class="navbar-form" role="search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search">
                    <span class="input-group-btn">
                      <button class="ori-btn ori-btn-default" type="button"><i style="padding:3px" class="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                  </div>
                </form>
              </div>
            </div>
          </div>
          {{-- <div class="collapse navbar-collapse">
            <form class="navbar-form" role="search">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-btn">
                <button type="submit" class="search-btn"> <span class="glyphicon glyphicon-search"> <span class="sr-only">Search</span> </span> </button>
                </span> </div>
            </form>
          </div> --}}
          <!--links--> 
        </div>
      </div>
    </div>
  </div>
</header>